import { defineCollection, z } from "astro:content"

export const collections = {
	général: defineCollection({
		type: "data",
		schema: z.object({
			nom: z.string(),
			codeLangue: z.string(),
			codeLongLangue: z.string(),
			séparateur: z.string(),
			auteur: z.object({
				site: z.string().url(),
				prénom: z.string(),
				nom: z.string(),
				nomComplet: z.string(),
				adresseMél: z.string().email(),
				description: z.string(),
				titreSite: z.string(),
				métier: z.string(),
				métierTexte: z.string().optional(),
				motsClés: z.string(),
				comptes: z.object({
					titre: z.string(),
					liste: z.array(z.object({
						image: z.string(),
						libellé: z.string(),
						alt: z.string(),
						lien: z.string().url().optional(),
					})),
				}),
			}),
			enTête: z.object({
				accueil: z.string(),
				liens: z.array(z.object({
					libellé: z.string(),
					adresse: z.string(),
				})),
				langues: z.array(z.object({
					libellé: z.string(),
					titre: z.string(),
					langue: z.string(),
				})),
			}),
			pied: z.object({
				date: z.string(),
				licence: z.string(),
				attribution: z.string(),
			}),
			libelléSiteWeb: z.string(),
			clickToInteract: z.string(),
			article: z.object({
				libelléProjets: z.string(),
				libelléCode: z.string(),
				libelléDémonstration: z.string(),
			}),
		}),
	}),
	accueil: defineCollection({
		type: "data",
		schema: z.object({
			id: z.string(),
			type: z.string(),
			description: z.string(),
			bonjour: z.string(),
			statut: z.string(),
			nomComplet: z.string(),
			liens: z.array(z.object({
				libellé: z.string(),
				adresse: z.string(),
			}))
		}),
	}),
	compétences: defineCollection({
		type: "data",
		schema: z.object({
			id: z.string(),
			type: z.string(),
			nom: z.string(),
			description: z.string(),
			liste: z.array(z.object({
				icône: z.string(),
				titre: z.string(),
				prioritaire: z.boolean(),
				description: z.string(),
				outils: z.array(z.object({
					image: z.string(),
					libellé: z.string(),
					alt: z.string(),
				})),
				services: z.array(z.object({
					image: z.string(),
					libellé: z.string(),
					alt: z.string(),
				})).optional(),
				lienProjets: z.string().optional(),
			})),
		}),
	}),
	formations: defineCollection({
		type: "data",
		schema: z.object({
			id: z.string(),
			nom: z.string(),
			description: z.string(),
			liste: z.array(z.object({
				id: z.string(),
				type: z.string(),
				titre: z.string(),
				organisation: z.string(),
				localisation: z.string(),
				carteIntégrée: z.string(),
				carte: z.string(),
				siteWeb: z.string(),
				documents: z.array(z.object({
					libellé: z.string(),
					adresse: z.string(),
				})),
				période: z.object({
					début: z.string(),
					fin: z.string(),
				}),
			})),
		}),
	}),
	projets: defineCollection({
		type: "data",
		schema: z.object({
			id: z.string(),
			nom: z.string(),
			description: z.string(),
			formulaireRecherche: z.object({
				titre: z.string(),
				champs: z.array(
					z.union([
						z.object({
							type: z.literal("select"),
							id: z.string(),
							nom: z.string(),
							libellé: z.string(),
							valeurs: z.union([
								z.array(z.string()),	
								z.array(
									z.union([
										z.string(),
										z.array(z.string()),
									]),
								),
							]),
						}),
						z.object({
							type: z.literal("text"),
							id: z.string(),
							nom: z.string(),
							libellé: z.string(),
							description: z.string(),
						}),
						z.object({
							type: z.literal("checkbox"),
							id: z.string(),
							nom: z.string(),
							libellé: z.string(),
							titre: z.string(),
						}),
					]),
				),
				messageValidation: z.string(),
				aucunRésultat: z.string(),
				résultat: z.string(),
			}),
			liste: z.array(z.object({
				id: z.string(),
				titre: z.string(),
				description: z.union([
					z.string(),
					z.array(z.string()),
				]),
				citation: z.object({
					déclaration: z.string(),
					auteur: z.string(),
				}).optional(),
				illustration: z.object({
					document: z.string(),
					titre: z.string(),
					description: z.string(),
				}),
				lienCode: z.string().optional(),
				lienDémonstration: z.string().optional(),
				motsClés: z.array(z.object({
					classe: z.enum(["domaine", "langage", "contexte", "équipe", "année"]),
					catégorie: z.string(),
					valeur: z.union([z.number(), z.string()]),
				})),
			})),
		}),
	}),
	expériences: defineCollection({
		type: "data",
		schema: z.object({
			id: z.string(),
			nom: z.string(),
			description: z.string(),
			liste: z.array(z.object({
				id: z.string(),
				type: z.string(),
				titre: z.string(),
				organisation: z.string(),
				localisation: z.string(),
				carteIntégrée: z.string(),
				carte: z.string(),
				description: z.string(),
				motsClés: z.array(z.object({
					classe: z.string(),
					catégorie: z.string(),
					valeur: z.string(),
				})),
				période: z.object({
					début: z.string(),
					fin: z.string(),
				}),
			})),
		}),
	}),
	àproposdemoi: defineCollection({
		type: "data",
		schema: z.object({
			id: z.string(),
			type: z.string(),
			nom: z.string(),
			description: z.string(),
			libellé: z.string(),
			descriptionPersonnelle: z.string(),
			langues: z.object({
				icône: z.string(),
				titre: z.string(),
				liste: z.array(z.object({
					image: z.string(),
					libellé: z.string(),
					alt: z.string(),
					niveau: z.string(),
				})),
			}),
			contributeur: z.object({
				titre: z.string(),
				liste: z.array(z.object({
					image: z.string(),
					libellé: z.string(),
					alt: z.string(),
				})),
			}),
		}),
	}),
	contact: defineCollection({
		type: "data",
		schema: z.object({
			id: z.string(),
			type: z.string(),
			nom: z.string(),
			description: z.string(),
			libellé: z.string(),
			adresse: z.object({
				titre: z.string(),
			}),
			formulaire: z.object({
				titre: z.string(),
				adresseEnvoi: z.string(),
				libelléEnvoi: z.string(),
				nom: z.object({
					description: z.string(),
					libellé: z.string(),
				}),
				adresseRéponse: z.object({
					description: z.string(),
					libellé: z.string(),
				}),
				sujet: z.object({
					description: z.string(),
					libellé: z.string(),
				}),
				message: z.object({
					description: z.string(),
					libellé: z.string(),
				}),
			}),
		}),
	}),
}
