import { useState } from "react"
import type { PropsWithChildren } from "react"

type ClientInteractiveMapProperties = {
	mapData: string
	mapLink: string
}

export default function ClientInteractiveMap({mapData, mapLink, children}: PropsWithChildren<ClientInteractiveMapProperties>) {
	const [isMapVisible, setIsMapVisible] = useState(false)
	const toggleIsMapVisible = () => setIsMapVisible(! isMapVisible)

	return <figure>
		<div className="iframe-envelope">
			<iframe src={`https://www.openstreetmap.org/export/embed.html?bbox=${mapData}&layer=mapnik`}></iframe>

			{
				isMapVisible || <div className="layer" onClick={toggleIsMapVisible}>
					{children}
				</div>
			}
		</div>

		<figcaption>
			<a href={`https://www.openstreetmap.org/#map=${mapLink}`}>
				Ouvrir dans un nouvel onglet
			</a>
		</figcaption>
	</figure>
}
