export const langues = {
	"fr": {
		"accueil": "accueil",
		"compétences": "compétences",
		"formations": "formations",
		"projets": "projets",
		"expériences": "expériences",
		"àproposdemoi": "àproposdemoi",
		"contact": "contact",
	},
	"en": {
		"home": "accueil",
		"skills": "compétences",
		"education": "formations",
		"projects": "projets",
		"experiences": "expériences",
		"aboutme": "àproposdemoi",
		"contact": "contact",
	},
	"eo": {
		"ĉefpaĝo": "accueil",
		"kompetentoj": "compétences",
		"instruaĵoj": "formations",
		"projektoj": "projets",
		"spertoj": "expériences",
		"primi": "àproposdemoi",
		"kontaktiĝo": "contact",
	},
}

export const équivalents = Object.entries(langues).reduce((équivalentsPages, [langue, listePages]) => {
	Object.entries(listePages).forEach(([nomPage, idPage]) => {
		(équivalentsPages as any)[idPage] = Object.assign({
			[langue]: nomPage,
		}, (équivalentsPages as any)[idPage] || {})
	})
	return équivalentsPages
}, {})
