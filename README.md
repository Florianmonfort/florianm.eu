# Portfolio

Reconstruction et remise au propre partielle de mon ancien portfolio avec <cite>Astro</cite>.

Ce projet sera remplacé par une refonte totale sur des bases propres (avec <cite>Tailwind</cite> en particulier).
